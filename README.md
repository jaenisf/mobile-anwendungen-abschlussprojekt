# Mobile-Anwendungen-Abschlussprojekt - Periodensystem der Elemente
In dieser Readme wollen wir die Funktionen unserer App vorstellen.   
Viel Spaß beim Ausprobieren wünschen  
Andreas Greiff und Florian Jänisch

## Auswahlmenü
![Auswahlmenü](./images/selection_menu.PNG)

Hierbei handelt es sich um das Einstiegsmenü und das erste Fenster der App.   
In dieser stehen dem Anwender vier Möglichkeiten zur Verfügung:
+   Vollständige Liste der chemischen Elemente anzeigen: Es öffnet sich die Übersicht aller Elemente
+   Elemente nach Gruppen gegliedert anzeigen: Es werden nur die Elemente zur jeweils gewählten Gruppe dargestellt
+   Elemente nach Perioden gegliedert anzeigen: Es werden nur die Elemente zur jeweils gewählten Periode dargestellt
+   Weitere Informationen: Über das Optionsmenü der App kann man auf die Webseite http://www.periodensystem.info/ gelangen, um sich dort weiter über das Periodensystem der Elemente zu informieren
+   About: Über das Optionsmenü der App kann man in das About-Fenster abspringen


## About
![Über](./images/about.PNG)

In diesem Fenster werden die Namen der Autoren sowie die beiden verwendeten APIs genannt.

## Vollständige Liste der chemischen Elemente
![Vollständige Liste der chemischen Elemente](./images/all_elements.PNG)

In diesem Fenster werden alle Elemente des Periodensystems dargestellt. Bei Klick auf eines der Elemente gelangt man in die Detailansicht desselben. Darüber hinaus stehen für die Liste noch Möglichkeiten der Sortierung sowie Möglichkeiten der Filterung zur Verfügung. Über die beiden Menüoptionen (1) und (2), kann gewählt werden, in wie vielen Spalten die Elemente dargestellt werden sollen.

### Sortieren der Liste
![Sortiermöglichkeiten](./images/sort.png)

Wie in der Übersicht gezeigt stehen die gezeigten Sortiermöglichkeiten zur Verfügung. Es wird jeweils die Option der auf- bzw. absteigenden Sortierung geboten.

### Filtern der Liste
![Filtermöglichkeiten](./images/filter.png)

Wie in der Übersicht gezeigt stehen die gezeigten Filtermöglichkeiten zur Verfügung. 

## Elemente nach Gruppen gegliedert
![Elemente nach Gruppen gegliedert](./images/group.PNG)

Hier werden die Elemente nach Gruppe gegliedert dargestellt. Als erste Gruppe wird hierbei die Gruppe eins gewählt. Mit einer Wischbewegung nach rechts wird in den Gruppen weiter voran geschritten. Bei einer Wischbewegung nach links, wird die vorhergehende Gruppe angezeigt. Nach links oder rechts kann aber auch über die Pfeile navigiert werden. Bei Klick auf eines der Elemente gelangt man in die Detailansicht desselben.

## Elemente nach Periode gegliedert
![Elemente nach Periode gegliedert](./images/period.PNG)

Hier werden die Elemente nach Periode gegliedert dargestellt. Als erste Periode wird hierbei die Periode eins gewählt. Mit einer Wischbewegung nach rechts wird in den Perioden weiter voran geschritten. Bei einer Wischbewegung nach links, wird die vorhergehende Periode angezeigt. Nach links oder rechts kann aber auch über die Pfeile navigiert werden. Bei Klick auf eines der Elemente gelangt man in die Detailansicht desselben.

## Detailansicht
![Detailansicht](./images/detail.PNG)

In dieser Ansicht wird ein einzelnes chemisches Element mitsamt seiner Eigenschaften dargestellt.

## Weitere Funktionen
Damit sich die App gut in das Android Ökosystem einbettet unterstützt die Anwendung sowohl den Tag/Nacht-Modus als auch mehrere Sprachen (Deutsch und Englisch).

### Vergleich Tagmodus zu Nachtmodus
![Vergleich Tagmodus zu Nachtmodus](./images/day_night.png)

In diesem Bild wird der Tagmodus (links) dem Nachtmodus (rechts) gegenübergestellt.
