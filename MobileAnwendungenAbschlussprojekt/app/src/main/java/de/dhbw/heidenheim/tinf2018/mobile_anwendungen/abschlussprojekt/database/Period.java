package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
@Entity(tableName = "PeriodTable")
public class Period {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "period_number")
    private int periodNumber;

    @ColumnInfo(name = "period_name")
    private String periodName;

    @ColumnInfo(name = "electron_shell")
    private String electronShell;

    @ColumnInfo(name = "language")
    private String language;

    public Period(int id, int periodNumber, String periodName, String electronShell, String language) {
        this.id = id;
        this.periodNumber = periodNumber;
        this.periodName = periodName;
        this.electronShell = electronShell;
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeriodNumber() {
        return periodNumber;
    }

    public String getPeriodName() {
        return periodName;
    }

    public String getElectronShell() {
        return electronShell;
    }

    public String getLanguage() {
        return language;
    }
}
