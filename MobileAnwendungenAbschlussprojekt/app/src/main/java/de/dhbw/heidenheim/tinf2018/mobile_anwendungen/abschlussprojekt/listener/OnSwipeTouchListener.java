package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.listener;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import org.jetbrains.annotations.NotNull;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class OnSwipeTouchListener implements View.OnTouchListener {
    private final GestureDetector gestureDetector;
    private final Context context;

    public OnSwipeTouchListener(Context context) {
        this.gestureDetector = new GestureDetector(context, new GestureListener());
        this.context = context;
    }

    @Override
    public boolean onTouch(@NotNull View v, MotionEvent event) {
        v.performClick();

        return this.getGestureDetector().onTouchEvent(event);
    }

    /**
     * Own Implementation of the Gesture Listener. This Lister is used for detect in which direction the user has swiped
     */
    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        /**
         * Calculating the swiping direction
         */
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;

            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();

                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight(context);
                        } else {
                            onSwipeLeft(context);
                        }

                        result = true;
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom(context);
                    } else {
                        onSwipeTop(context);
                    }

                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            return result;
        }
    }

    public void onSwipeRight(Context context) {
    }

    public void onSwipeLeft(Context context) {
    }

    public void onSwipeTop(Context context) {
    }

    public void onSwipeBottom(Context context) {
    }

    public GestureDetector getGestureDetector() {
        return gestureDetector;
    }

    public Context getContext() {
        return context;
    }
}