package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.PeriodicTableOfElements;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.ChemicalElement;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class DetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_detail);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        this.showElementDetails();
    }

    private void showElementDetails() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            int atomicNumberChemicalElement = Integer.parseInt(bundle.getString("atomicNumber", ""));

            if (atomicNumberChemicalElement > 0) {
                ChemicalElement chemicalElement = PeriodicTableOfElements.getInstance().getChemicalElementByAtomicNumber(atomicNumberChemicalElement);

                this.showElementDetailsOverview(chemicalElement);
                this.showElementDetailsTable(chemicalElement);
            }
        }
    }

    /**
     * sets the data for the chemical elements overview
     */
    @SuppressLint("SetTextI18n")
    private void showElementDetailsOverview(ChemicalElement chemicalElement) {
        View v = this.findViewById(R.id.overviewElement);

        FrameLayout frameLayout = v.findViewById(R.id.chemicalElementFrameLayout);

        PeriodicTableOfElements.setChemicalElementBackgroundCpkHexColor(frameLayout, chemicalElement);

        if (chemicalElement.getAtomicNumber() > 0) {
            ((TextView) v.findViewById(R.id.atomicNumberTextView)).setText(Integer.toString(chemicalElement.getAtomicNumber()));
        } else {
            ((TextView) v.findViewById(R.id.atomicNumberTextView)).setText("-");
        }

        if (chemicalElement.getElementSymbol() != null && !chemicalElement.getElementSymbol().isEmpty()) {
            ((TextView) v.findViewById(R.id.elementSymbolTextView)).setText(chemicalElement.getElementSymbol());
        } else {
            ((TextView) v.findViewById(R.id.elementSymbolTextView)).setText("-");
        }

        if (chemicalElement.getElementName() != null && !chemicalElement.getElementName().isEmpty()) {
            ((TextView) v.findViewById(R.id.elementNameTextView)).setText(chemicalElement.getElementName());
        } else {
            ((TextView) v.findViewById(R.id.elementNameTextView)).setText("-");
        }

        if (chemicalElement.getAtomicMass() != null && !chemicalElement.getAtomicMass().isEmpty()) {
            ((TextView) v.findViewById(R.id.atomicMassTextView)).setText(this.getString(R.string.TableAtomicMass,
                    new DecimalFormat("#.#####").format(Float.parseFloat(chemicalElement.getAtomicMass()))));
        } else {
            ((TextView) v.findViewById(R.id.atomicMassTextView)).setText("-");
        }
    }

    /**
     * sets the data for the chemical elements detail table
     */
    @SuppressLint("SetTextI18n")
    private void showElementDetailsTable(ChemicalElement chemicalElement) {
        if (chemicalElement.getAtomicNumber() > 0) {
            ((TextView) this.findViewById(R.id.tableAtomicNumber)).setText(Integer.toString(chemicalElement.getAtomicNumber()));
        } else {
            ((TextView) this.findViewById(R.id.tableAtomicNumber)).setText("-");
        }

        if (chemicalElement.getElementSymbol() != null && !chemicalElement.getElementSymbol().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableElementsSymbol)).setText(chemicalElement.getElementSymbol());
        } else {
            ((TextView) this.findViewById(R.id.tableElementsSymbol)).setText("-");
        }

        if (chemicalElement.getElementName() != null && !chemicalElement.getElementName().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableElementName)).setText(chemicalElement.getElementName());
        } else {
            ((TextView) this.findViewById(R.id.tableElementName)).setText("-");
        }

        if (chemicalElement.getPeriod() != null && !chemicalElement.getPeriod().isEmpty()) {
            ((TextView) this.findViewById(R.id.tablePeriod)).setText(chemicalElement.getPeriod());
        } else {
            ((TextView) this.findViewById(R.id.tablePeriod)).setText("-");
        }

        if (chemicalElement.getGroup() != null && !chemicalElement.getGroup().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableGroup)).setText(chemicalElement.getGroup());
        } else {
            ((TextView) this.findViewById(R.id.tableGroup)).setText("-");
        }

        if (chemicalElement.getType() != null && !chemicalElement.getType().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableType)).setText(chemicalElement.getType());
        } else {
            ((TextView) this.findViewById(R.id.tableType)).setText("-");
        }

        if (chemicalElement.getDiscoverer() != null && !chemicalElement.getDiscoverer().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableDiscoverer)).setText(chemicalElement.getDiscoverer());
        } else {
            ((TextView) this.findViewById(R.id.tableDiscoverer)).setText("-");
        }

        if (chemicalElement.getYear() != null && !chemicalElement.getYear().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableYear)).setText(chemicalElement.getYear());
        } else {
            ((TextView) this.findViewById(R.id.tableYear)).setText("-");
        }

        if (chemicalElement.getAtomicMass() != null && !chemicalElement.getAtomicMass().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableAtomicMass)).setText(this.getString(R.string.TableAtomicMass,
                    new DecimalFormat("#.#####").format(Float.parseFloat(chemicalElement.getAtomicMass()))));
        } else {
            ((TextView) this.findViewById(R.id.tableAtomicMass)).setText("-");
        }

        if (chemicalElement.getAtomicRadius() != null && !chemicalElement.getAtomicRadius().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableAtomicRadius)).setText(chemicalElement.getAtomicRadius());
        } else {
            ((TextView) this.findViewById(R.id.tableAtomicRadius)).setText("-");
        }

        if (chemicalElement.getBondingType() != null && !chemicalElement.getBondingType().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableBondingType)).setText(chemicalElement.getBondingType().substring(0, 1).toUpperCase() + chemicalElement.getBondingType().substring(1));
        } else {
            ((TextView) this.findViewById(R.id.tableBondingType)).setText("-");
        }

        if (chemicalElement.getElectronegativity() != null && !chemicalElement.getElectronegativity().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableElectronegativity)).setText(this.getString(R.string.TableElectronegativity,
                    new DecimalFormat("#.#####").format(Float.parseFloat(chemicalElement.getElectronegativity()))));
        } else {
            ((TextView) this.findViewById(R.id.tableElectronegativity)).setText("-");
        }

        if (chemicalElement.getNumberOfIsotopes() != null && !chemicalElement.getNumberOfIsotopes().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfIsotopes)).setText(chemicalElement.getNumberOfIsotopes());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfIsotopes)).setText("-");
        }

        if (chemicalElement.getNumberOfElectrons() != null && !chemicalElement.getNumberOfElectrons().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfElectrons)).setText(chemicalElement.getNumberOfElectrons());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfElectrons)).setText("-");
        }

        if (chemicalElement.getNumberOfNeutrons() != null && !chemicalElement.getNumberOfNeutrons().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfNeutrons)).setText(chemicalElement.getNumberOfNeutrons());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfNeutrons)).setText("-");
        }

        if (chemicalElement.getNumberOfProtons() != null && !chemicalElement.getNumberOfProtons().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfProtons)).setText(chemicalElement.getNumberOfProtons());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfProtons)).setText("-");
        }

        if (chemicalElement.getNumberOfShells() != null && !chemicalElement.getNumberOfShells().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfShells)).setText(chemicalElement.getNumberOfShells());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfShells)).setText("-");
        }

        if (chemicalElement.getNumberOfValence() != null && !chemicalElement.getNumberOfValence().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableNumberOfValence)).setText(chemicalElement.getNumberOfValence());
        } else {
            ((TextView) this.findViewById(R.id.tableNumberOfValence)).setText("-");
        }

        if (chemicalElement.getRadioactive() != null && !chemicalElement.getRadioactive().isEmpty()) {
            if (chemicalElement.getRadioactive().equals("yes")) {
                ((TextView) this.findViewById(R.id.tableRadioactivity)).setText(Html.fromHtml("&#x2713;", 0));
            } else if (chemicalElement.getRadioactive().equals("no")) {
                ((TextView) this.findViewById(R.id.tableRadioactivity)).setText(Html.fromHtml("&#x2718;", 0));
            } else {
                ((TextView) this.findViewById(R.id.tableRadioactivity)).setText(chemicalElement.getRadioactive());
            }
        } else {
            ((TextView) this.findViewById(R.id.tableRadioactivity)).setText("-");
        }

        if (chemicalElement.getMetal() != null && !chemicalElement.getMetal().isEmpty()) {
            if (chemicalElement.getMetal().equals("yes")) {
                ((TextView) this.findViewById(R.id.tableMetal)).setText(Html.fromHtml("&#x2713;", 0));
            } else if (chemicalElement.getMetal().equals("no")) {
                ((TextView) this.findViewById(R.id.tableMetal)).setText(Html.fromHtml("&#x2718;", 0));
            } else {
                ((TextView) this.findViewById(R.id.tableMetal)).setText(chemicalElement.getMetal());
            }
        } else {
            ((TextView) this.findViewById(R.id.tableMetal)).setText("-");
        }

        if (chemicalElement.getMetalloid() != null && !chemicalElement.getMetalloid().isEmpty()) {
            if (chemicalElement.getMetalloid().equals("yes")) {
                ((TextView) this.findViewById(R.id.tableMetalloid)).setText(Html.fromHtml("&#x2713;", 0));
            } else if (chemicalElement.getMetalloid().equals("no")) {
                ((TextView) this.findViewById(R.id.tableMetalloid)).setText(Html.fromHtml("&#x2718;", 0));
            } else {
                ((TextView) this.findViewById(R.id.tableMetalloid)).setText(chemicalElement.getMetalloid());
            }
        } else {
            ((TextView) this.findViewById(R.id.tableMetalloid)).setText("-");
        }

        if (chemicalElement.getNatural() != null && !chemicalElement.getNatural().isEmpty()) {
            if (chemicalElement.getNatural().equals("yes")) {
                ((TextView) this.findViewById(R.id.tableNatural)).setText(Html.fromHtml("&#x2713;", 0));
            } else if (chemicalElement.getNatural().equals("no")) {
                ((TextView) this.findViewById(R.id.tableNatural)).setText(Html.fromHtml("&#x2718;", 0));
            } else {
                ((TextView) this.findViewById(R.id.tableNatural)).setText(chemicalElement.getNatural());
            }
        } else {
            ((TextView) this.findViewById(R.id.tableNatural)).setText("-");
        }

        if (chemicalElement.getNonmetal() != null && !chemicalElement.getNonmetal().isEmpty()) {
            if (chemicalElement.getNonmetal().equals("yes")) {
                ((TextView) this.findViewById(R.id.tableNonmetal)).setText(Html.fromHtml("&#x2713;", 0));
            } else if (chemicalElement.getNonmetal().equals("no")) {
                ((TextView) this.findViewById(R.id.tableNonmetal)).setText(Html.fromHtml("&#x2718;", 0));
            } else {
                ((TextView) this.findViewById(R.id.tableNonmetal)).setText(chemicalElement.getNonmetal());
            }
        } else {
            ((TextView) this.findViewById(R.id.tableNonmetal)).setText("-");
        }

        if (chemicalElement.getMeltingPoint() != null && !chemicalElement.getMeltingPoint().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableMeltingPoint)).setText(this.getString(R.string.TableMeltingPoint,
                    new DecimalFormat("#.####").format(Float.parseFloat(chemicalElement.getMeltingPoint()))));
        } else {
            ((TextView) this.findViewById(R.id.tableMeltingPoint)).setText("-");
        }

        if (chemicalElement.getBoilingPoint() != null && !chemicalElement.getBoilingPoint().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableBoilingPoint)).setText(this.getString(R.string.TableBoilingPoint,
                    new DecimalFormat("#.####").format(Float.parseFloat(chemicalElement.getBoilingPoint()))));
        } else {
            ((TextView) this.findViewById(R.id.tableBoilingPoint)).setText("-");
        }

        if (chemicalElement.getDensity() != null && !chemicalElement.getDensity().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableDensity)).setText(this.getString(R.string.TableDensity,
                    new DecimalFormat("#.#######").format(Float.parseFloat(chemicalElement.getDensity()))));
        } else {
            ((TextView) this.findViewById(R.id.tableDensity)).setText("-");
        }

        if (chemicalElement.getPhase() != null && !chemicalElement.getPhase().isEmpty()) {
            ((TextView) this.findViewById(R.id.tablePhase)).setText(chemicalElement.getPhase().substring(0, 1).toUpperCase() + chemicalElement.getPhase().substring(1));
        } else {
            ((TextView) this.findViewById(R.id.tablePhase)).setText("-");
        }

        if (chemicalElement.getIonizationEnergy() != null && !chemicalElement.getIonizationEnergy().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableIonizationEnergy)).setText(this.getString(R.string.TableIonizationEnergy, chemicalElement.getIonizationEnergy()));
        } else {
            ((TextView) this.findViewById(R.id.tableIonizationEnergy)).setText("-");
        }

        if (chemicalElement.getOxidationStates() != null && !chemicalElement.getOxidationStates().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableOxidationStates)).setText(chemicalElement.getOxidationStates());
        } else {
            ((TextView) this.findViewById(R.id.tableOxidationStates)).setText("-");
        }

        if (chemicalElement.getStandardSate() != null && !chemicalElement.getStandardSate().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableStandardStates)).setText(chemicalElement.getStandardSate().substring(0, 1).toUpperCase() + chemicalElement.getStandardSate().substring(1));
        } else {
            ((TextView) this.findViewById(R.id.tableStandardStates)).setText("-");
        }

        if (chemicalElement.getVanDelWaalsRadius() != null && !chemicalElement.getVanDelWaalsRadius().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableVanDelWaalsRadius)).setText(this.getString(R.string.TableVanDelWaalsRadius, chemicalElement.getVanDelWaalsRadius()));
        } else {
            ((TextView) this.findViewById(R.id.tableVanDelWaalsRadius)).setText("-");
        }

        if (chemicalElement.getSpecificHeat() != null && !chemicalElement.getSpecificHeat().isEmpty()) {
            ((TextView) this.findViewById(R.id.tableSpecificHeat)).setText(this.getString(R.string.TableSpecificHeat,
                    new DecimalFormat("#.###").format(Float.parseFloat(chemicalElement.getSpecificHeat()))));
        } else {
            ((TextView) this.findViewById(R.id.tableSpecificHeat)).setText("-");
        }
    }
}