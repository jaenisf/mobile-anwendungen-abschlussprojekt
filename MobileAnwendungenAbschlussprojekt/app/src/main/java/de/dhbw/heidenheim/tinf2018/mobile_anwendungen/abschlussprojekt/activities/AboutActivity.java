package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_about);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onStart() {
        super.onStart();

        this.getImageWithGlideAndRoundedCorners(this.findViewById(R.id.gif));
    }

    /**
     * In this Method the gif is set to the pl.droidsonroids.gif.GifImageView
     */
    private void getImageWithGlideAndRoundedCorners(ImageView imageView) {
        Glide.with(this.getApplicationContext())
                .load(R.drawable.atom_animated)
                .error(R.drawable.error)
                .placeholder(R.drawable.white)
                .apply(RequestOptions.bitmapTransform(new jp.wasabeef.glide.transformations.RoundedCornersTransformation(30, 15)))
                .into(imageView);
    }
}