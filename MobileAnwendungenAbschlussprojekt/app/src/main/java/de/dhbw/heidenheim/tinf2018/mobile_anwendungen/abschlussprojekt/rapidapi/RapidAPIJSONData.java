package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class RapidAPIJSONData {
    @SerializedName("AtomicMass")
    private String atomicMass;

    @SerializedName("AtomicNumber")
    private int atomicNumber;

    @SerializedName("AtomicRadius")
    private String atomicRadius;

    @SerializedName("BoilingPoint")
    private String boilingPoint;

    @SerializedName("Density")
    private String density;

    @SerializedName("Discoverer")
    private String discoverer;

    @SerializedName("Electronegativity")
    private String electronegativity;

    @SerializedName("Element")
    private String element;

    @SerializedName("FirstIonization")
    private String firstIonization;

    @SerializedName("Group")
    private String group;

    @SerializedName("MeltingPoint")
    private String meltingPoint;

    @SerializedName("Metal")
    private String metal;

    @SerializedName("Metalloid")
    private String metalloid;

    @SerializedName("Natural")
    private String natural;

    @SerializedName("Nonmetal")
    private String nonmetal;

    @SerializedName("NumberOfIsotopes")
    private String numberOfIsotopes;

    @SerializedName("NumberofElectrons")
    private String numberofElectrons;

    @SerializedName("NumberofNeutrons")
    private String numberofNeutrons;

    @SerializedName("NumberofProtons")
    private String numberofProtons;

    @SerializedName("NumberofShells")
    private String numberofShells;

    @SerializedName("NumberofValence")
    private String numberofValence;

    @SerializedName("Period")
    private String period;

    @SerializedName("Phase")
    private String phase;

    @SerializedName("Radioactive")
    private String radioactive;

    @SerializedName("SpecificHeat")
    private String specificHeat;

    @SerializedName("Symbol")
    private String symbol;

    @SerializedName("Type")
    private String type;

    @SerializedName("Year")
    private String year;

    public RapidAPIJSONData(String atomicMass, int atomicNumber, String atomicRadius, String boilingPoint, String density, String discoverer, String electronegativity, String element, String firstIonization, String group, String meltingPoint, String metal, String metalloid, String natural, String nonmetal, String numberOfIsotopes, String numberofElectrons, String numberofNeutrons, String numberofProtons, String numberofShells, String numberofValence, String period, String phase, String radioactive, String specificHeat, String symbol, String type, String year) {
        this.atomicMass = atomicMass;
        this.atomicNumber = atomicNumber;
        this.atomicRadius = atomicRadius;
        this.boilingPoint = boilingPoint;
        this.density = density;
        this.discoverer = discoverer;
        this.electronegativity = electronegativity;
        this.element = element;
        this.firstIonization = firstIonization;
        this.group = group;
        this.meltingPoint = meltingPoint;
        this.metal = metal;
        this.metalloid = metalloid;
        this.natural = natural;
        this.nonmetal = nonmetal;
        this.numberOfIsotopes = numberOfIsotopes;
        this.numberofElectrons = numberofElectrons;
        this.numberofNeutrons = numberofNeutrons;
        this.numberofProtons = numberofProtons;
        this.numberofShells = numberofShells;
        this.numberofValence = numberofValence;
        this.period = period;
        this.phase = phase;
        this.radioactive = radioactive;
        this.specificHeat = specificHeat;
        this.symbol = symbol;
        this.type = type;
        this.year = year;
    }

    public String getAtomicMass() {
        return atomicMass;
    }

    public void setAtomicMass(String atomicMass) {
        this.atomicMass = atomicMass;
    }

    public int getAtomicNumber() {
        return atomicNumber;
    }

    public void setAtomicNumber(int atomicNumber) {
        this.atomicNumber = atomicNumber;
    }

    public String getAtomicRadius() {
        return atomicRadius;
    }

    public void setAtomicRadius(String atomicRadius) {
        this.atomicRadius = atomicRadius;
    }

    public String getBoilingPoint() {
        return boilingPoint;
    }

    public void setBoilingPoint(String boilingPoint) {
        this.boilingPoint = boilingPoint;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getDiscoverer() {
        return discoverer;
    }

    public void setDiscoverer(String discoverer) {
        this.discoverer = discoverer;
    }

    public String getElectronegativity() {
        return electronegativity;
    }

    public void setElectronegativity(String electronegativity) {
        this.electronegativity = electronegativity;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getFirstIonization() {
        return firstIonization;
    }

    public void setFirstIonization(String firstIonization) {
        this.firstIonization = firstIonization;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getMeltingPoint() {
        return meltingPoint;
    }

    public void setMeltingPoint(String meltingPoint) {
        this.meltingPoint = meltingPoint;
    }

    public String getMetal() {
        return metal;
    }

    public void setMetal(String metal) {
        this.metal = metal;
    }

    public String getMetalloid() {
        return metalloid;
    }

    public void setMetalloid(String metalloid) {
        this.metalloid = metalloid;
    }

    public String getNatural() {
        return natural;
    }

    public void setNatural(String natural) {
        this.natural = natural;
    }

    public String getNonmetal() {
        return nonmetal;
    }

    public void setNonmetal(String nonmetal) {
        this.nonmetal = nonmetal;
    }

    public String getNumberOfIsotopes() {
        return numberOfIsotopes;
    }

    public void setNumberOfIsotopes(String numberOfIsotopes) {
        this.numberOfIsotopes = numberOfIsotopes;
    }

    public String getNumberofElectrons() {
        return numberofElectrons;
    }

    public void setNumberofElectrons(String numberofElectrons) {
        this.numberofElectrons = numberofElectrons;
    }

    public String getNumberofNeutrons() {
        return numberofNeutrons;
    }

    public void setNumberofNeutrons(String numberofNeutrons) {
        this.numberofNeutrons = numberofNeutrons;
    }

    public String getNumberofProtons() {
        return numberofProtons;
    }

    public void setNumberofProtons(String numberofProtons) {
        this.numberofProtons = numberofProtons;
    }

    public String getNumberofShells() {
        return numberofShells;
    }

    public void setNumberofShells(String numberofShells) {
        this.numberofShells = numberofShells;
    }

    public String getNumberofValence() {
        return numberofValence;
    }

    public void setNumberofValence(String numberofValence) {
        this.numberofValence = numberofValence;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getRadioactive() {
        return radioactive;
    }

    public void setRadioactive(String radioactive) {
        this.radioactive = radioactive;
    }

    public String getSpecificHeat() {
        return specificHeat;
    }

    public void setSpecificHeat(String specificHeat) {
        this.specificHeat = specificHeat;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}