package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model;

import android.annotation.SuppressLint;

import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities.DetailActivity;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class ChemicalElementAdapter extends RecyclerView.Adapter<ChemicalElementAdapter.ChemicalElementViewHolder> {
    private ArrayList<ChemicalElement> chemicalElementArrayList;
    private Context context;

    public ChemicalElementAdapter(ArrayList<ChemicalElement> chemicalElementArrayList, Context context) {
        this.chemicalElementArrayList = chemicalElementArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ChemicalElementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ChemicalElementViewHolder chemicalElementViewHolder = new ChemicalElementViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chemical_element, parent, false));
        chemicalElementViewHolder.setContext(context);
        return chemicalElementViewHolder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ChemicalElementViewHolder holder, int position) {
        ChemicalElement chemicalElement = chemicalElementArrayList.get(position);

        PeriodicTableOfElements.setChemicalElementBackgroundCpkHexColor(holder.getChemicalElement(), chemicalElement);

        holder.getAtomicNumber().setText(Integer.toString(chemicalElement.getAtomicNumber()));
        holder.getElementSymbol().setText(chemicalElement.getElementSymbol());
        holder.getElementName().setText(chemicalElement.getElementName());
        holder.getAtomicMass().setText(new DecimalFormat("#.###").format(Float.parseFloat(chemicalElement.getAtomicMass())));
    }

    @Override
    public int getItemCount() {
        return chemicalElementArrayList.size();
    }

    public static class ChemicalElementViewHolder extends RecyclerView.ViewHolder {
        private Context context;

        private FrameLayout chemicalElement;

        private TextView atomicNumber;
        private TextView elementSymbol;
        private TextView elementName;
        private TextView atomicMass;

        @SuppressLint("CutPasteId")
        public ChemicalElementViewHolder(@NonNull View itemView) {
            super(itemView);

            this.setChemicalElement(itemView.findViewById(R.id.chemicalElementFrameLayout));
            this.setAtomicNumber(itemView.findViewById(R.id.atomicNumberTextView));
            this.setElementSymbol(itemView.findViewById(R.id.elementSymbolTextView));
            this.setElementName(itemView.findViewById(R.id.elementNameTextView));
            this.setAtomicMass(itemView.findViewById(R.id.atomicMassTextView));

            itemView.setOnClickListener(v -> {
                if (this.getContext() == null) {
                    return;
                }

                this.getContext().startActivity(new Intent(this.getContext(), DetailActivity.class).putExtra("atomicNumber", ((TextView) v.findViewById(R.id.atomicNumberTextView)).getText()));
            });
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

        public FrameLayout getChemicalElement() {
            return chemicalElement;
        }

        public void setChemicalElement(FrameLayout chemicalElement) {
            this.chemicalElement = chemicalElement;
        }

        public TextView getAtomicNumber() {
            return atomicNumber;
        }

        public void setAtomicNumber(TextView atomicNumber) {
            this.atomicNumber = atomicNumber;
        }

        public TextView getElementSymbol() {
            return elementSymbol;
        }

        public void setElementSymbol(TextView elementSymbol) {
            this.elementSymbol = elementSymbol;
        }

        public TextView getElementName() {
            return elementName;
        }

        public void setElementName(TextView elementName) {
            this.elementName = elementName;
        }

        public TextView getAtomicMass() {
            return atomicMass;
        }

        public void setAtomicMass(TextView atomicMass) {
            this.atomicMass = atomicMass;
        }
    }
}