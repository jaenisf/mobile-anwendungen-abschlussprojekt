package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.util.Locale;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database.*;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.PeriodicTableOfElements;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.listener.OnSwipeTouchListener;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class GroupActivity extends AppCompatActivity {
    private int currentGroupNumber = 1;
    private GroupDao groupDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_group);

        this.initializeDatabase();
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.showChemicalElementsByGroup();

        this.findViewById(R.id.scrollView).setOnTouchListener(new OnSwipeTouchListener(GroupActivity.this) {
            public void onSwipeRight(Context context) {
                derementCurrentGroupNumber();
            }

            public void onSwipeLeft(Context context) {
                incrementCurrentGroupNumber();
            }

            public void onSwipeTop(Context context) {}

            public void onSwipeBottom(Context context) {}
        });

        this.findViewById(R.id.arrowLeft).setOnClickListener(v -> derementCurrentGroupNumber());
        this.findViewById(R.id.arrowRight).setOnClickListener(v -> incrementCurrentGroupNumber());
    }

    private void initializeDatabase() {
        // Create Database Objects
        GroupDatabase groupDatabase = Room.databaseBuilder(this, GroupDatabase.class, "GroupTable").allowMainThreadQueries().build();
        this.setGroupDao(groupDatabase.groupDao());

        // If database is empty, enter standard values

        // SET false TO true | only needed, when db values changed --> to clear database table
        if (false) {
            this.getGroupDao().deleteAllEntries();
        }

        if (this.getGroupDao().getAllGroupInfo() == null || this.getGroupDao().getAllGroupInfo().isEmpty()) {
            this.insertStandardDatabaseValues();
        }
    }

    private void showChemicalElementsByGroup(int groupNumber) {
        PeriodicTableOfElements.getInstance().showChemicalElementsByGroupInRecyclerView(this, this.findViewById(R.id.recyclerView), this.findViewById(R.id.group), this.findViewById(R.id.errorTextView), groupNumber);
    }

    /**
     * show the header depending of the selected group id and system language
     */
    @SuppressLint("SetTextI18n")
    private void showChemicalElementsByGroup() {
        String used_language = "en";

        if (Locale.getDefault().getDisplayLanguage().equals("Deutsch")) {
            used_language = "de";
        }

        Group group = this.getGroupDao().getSpecificGroup(this.getCurrentGroupNumber(), used_language);

        Log.d("Group", Integer.toString(this.getCurrentGroupNumber()));
        ((TextView) this.findViewById(R.id.textViewTitle)).setText(group.getGroupName() + "\n (" + group.getGroupType() + ")");

        this.showChemicalElementsByGroup(this.getCurrentGroupNumber());
    }

    /**
     * increment the group ID
     */
    private void incrementCurrentGroupNumber() {
        if (this.getCurrentGroupNumber() == 18) {
            this.setCurrentGroupNumber(1);
        } else {
            this.setCurrentGroupNumber(getCurrentGroupNumber() + 1);
        }

        this.showChemicalElementsByGroup();
    }

    /**
     * decrement the group ID
     */
    private void derementCurrentGroupNumber() {
        if (this.getCurrentGroupNumber() == 1) {
            this.setCurrentGroupNumber(18);
        } else {
            this.setCurrentGroupNumber(getCurrentGroupNumber() - 1);
        }

        this.showChemicalElementsByGroup();
    }

    private int getCurrentGroupNumber() {
        return currentGroupNumber;
    }

    private void setCurrentGroupNumber(int currentGroupNumber) {
        this.currentGroupNumber = currentGroupNumber;
    }

    public GroupDao getGroupDao() {
        return groupDao;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    /**
     * Inserting the needed group definitions into the database
     */
    private void insertStandardDatabaseValues() {
        Group group;

        // Groups - Language: German
        group = new Group(1, 1, "Alkalimetalle", "1. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(2, 2, "Erdalkalimetalle", "2. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(3, 3, "Scandiumgruppe", "3. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(4, 4, "Titangruppe", "4. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(5, 5, "Vanadiumgruppe", "5. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(6, 6, "Chromgruppe", "6. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(7, 7, "Mangangruppe", "7. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(8, 8, "Eisengruppe", "8. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(9, 9, "Cobaltgruppe", "8. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(10, 10, "Nickelgruppe", "8. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(11, 11, "Kupfergruppe", "1. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(12, 12, "Zinkgruppe", "2. Nebengruppe", "de");
        groupDao.insert(group);
        group = new Group(13, 13, "Borgruppe", "3. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(14, 14, "Kohlenstoff-Silicium-Gruppe", "4. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(15, 15, "Stickstoff-Phosphor-Gruppe", "5. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(16, 16, "Chalkogene", "6. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(17, 17, "Halogene", "7. Hauptgruppe", "de");
        groupDao.insert(group);
        group = new Group(18, 18, "Edelgase", "8. Hauptgruppe", "de");
        groupDao.insert(group);

        // Groups - Language: English
        group = new Group(19, 1, "Alkali Metals", "1st Main Group", "en");
        groupDao.insert(group);
        group = new Group(20, 2, "Alkaline Earth Metals", "2nd Main Group", "en");
        groupDao.insert(group);
        group = new Group(21, 3, "Scandium Group", "3rd Subgroup", "en");
        groupDao.insert(group);
        group = new Group(22, 4, "Titanium Group", "4th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(23, 5, "Vanadium Group", "5th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(24, 6, "Chrome Group", "6th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(25, 7, "Manganese Group", "7th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(26, 8, "Iron Group", "8th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(27, 9, "Cobalt Group", "8th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(28, 10, "Nickel Group", "8th Subgroup", "en");
        groupDao.insert(group);
        group = new Group(29, 11, "Copper Group", "1st Subgroup", "en");
        groupDao.insert(group);
        group = new Group(30, 12, "Zinc Group", "2nd Subgroup", "en");
        groupDao.insert(group);
        group = new Group(31, 13, "Boron group", "3rd Main Group", "en");
        groupDao.insert(group);
        group = new Group(32, 14, "Carbon-Silicon Group", "4th Main Group", "en");
        groupDao.insert(group);
        group = new Group(33, 15, "Nitrogen Phosphorus Group", "5th Main Group", "en");
        groupDao.insert(group);
        group = new Group(34, 16, "Chalcogens", "6th Main Group", "en");
        groupDao.insert(group);
        group = new Group(35, 17, "Halogens", "7th Main Group", "en");
        groupDao.insert(group);
        group = new Group(36, 18, "Noble Gases", "8th Main Group", "en");
        groupDao.insert(group);
    }
}