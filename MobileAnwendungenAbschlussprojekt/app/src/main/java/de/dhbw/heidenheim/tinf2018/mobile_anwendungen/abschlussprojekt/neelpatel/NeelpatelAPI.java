package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.neelpatel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public interface NeelpatelAPI {
    @GET(".")
    Call<ArrayList<NeelpatelJSONData>> getChemicalElements();
}