package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
@Entity(tableName = "GroupTable")
public class Group {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "group_number")
    private int groupNumber;

    @ColumnInfo(name = "group_name")
    private String groupName;

    @ColumnInfo(name = "group_type")
    private String groupType;

    @ColumnInfo(name = "language")
    private String language;

    public Group(int id, int groupNumber, String groupName, String groupType, String language) {
        this.id = id;
        this.groupNumber = groupNumber;
        this.groupName = groupName;
        this.groupType = groupType;
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public String getLanguage() {
        return language;
    }
}