package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.function.Predicate;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.ChemicalElement;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.PeriodicTableOfElements;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class ElementsActivity extends AppCompatActivity {
    private String sorting = "";
    private String filtering = "";
    private int spanCount = 1;

    private final static String SORTING_SHARED_PREFERENCES = "SortingSharedPreferencesValue";
    private final static String FILTERING_SHARED_PREFERENCES = "FilteringSharedPreferencesValue";
    private final static String SPAN_COUNT_SHARED_PREFERENCES = "SpanCountSharedPreferencesValue";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(ElementsActivity.class.getSimpleName(), "onCreate");
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_elements);

        this.setSpanCount(ElementsActivity.getSpanCountSharedPreferencesValue(this));
    }

    private AdapterView.OnItemSelectedListener onItemSelectedListenerSorting = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, @NotNull View view, int position, long id) {
            Log.d(ElementsActivity.class.getSimpleName(), "onItemSelectedListenerSorting - onItemSelected");

            for (int i = 0; i < view.getResources().getStringArray(R.array.SortingArray).length; i++) {
                if (parent.getItemAtPosition(position).toString().equals(view.getResources().getStringArray(R.array.SortingArray)[i])) {
                    setSorting(parent.getItemAtPosition(position).toString());
                    setSortingSharedPreferencesValue((Activity) view.getContext(), parent.getItemAtPosition(position).toString());
                    updateRecyclerView();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Log.d(ElementsActivity.class.getSimpleName(), "onItemSelectedListenerSorting - onNothingSelected");
        }
    };

    private AdapterView.OnItemSelectedListener onItemSelectedListenerFiltering = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Log.d(ElementsActivity.class.getSimpleName(), "onItemSelectedListenerFiltering - onItemSelected");

            for (int i = 0; i < view.getResources().getStringArray(R.array.FilteringArray).length; i++) {
                if (parent.getItemAtPosition(position).toString().equals(view.getResources().getStringArray(R.array.FilteringArray)[i])) {
                    setFiltering(parent.getItemAtPosition(position).toString());
                    setFilteringSharedPreferencesValue((Activity) view.getContext(), parent.getItemAtPosition(position).toString());
                    updateRecyclerView();
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            Log.d(ElementsActivity.class.getSimpleName(), "onItemSelectedListenerFiltering - onNothingSelected");
        }
    };

    @Override
    protected void onStart() {
        Log.d(ElementsActivity.class.getSimpleName(), "onStart");

        super.onStart();

        this.initializeSpinnerSorting();
        this.initializeSpinnerFiltering();
    }

    private void initializeSpinnerSorting() {
        Log.d(ElementsActivity.class.getSimpleName(), "initializeSpinnerSorting");

        ArrayAdapter<CharSequence> charSequenceArrayAdapterSorting = ArrayAdapter.createFromResource(this, R.array.SortingArray, android.R.layout.simple_spinner_item);
        charSequenceArrayAdapterSorting.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinnerSorting = this.findViewById(R.id.spinnerSorting);
        spinnerSorting.setAdapter(charSequenceArrayAdapterSorting);
        spinnerSorting.setOnItemSelectedListener(this.getOnItemSelectedListenerSorting());

        if (!ElementsActivity.getSortingSharedPreferencesValue(this).isEmpty()) {
            for (int i = 0; i < this.getResources().getStringArray(R.array.SortingArray).length; i++) {
                if (ElementsActivity.getSortingSharedPreferencesValue(this).equals(spinnerSorting.getResources().getStringArray(R.array.SortingArray)[i])) {
                    spinnerSorting.setSelection(i);
                }
            }
        } else {
            spinnerSorting.setSelection(0);
        }
    }

    private void initializeSpinnerFiltering() {
        Log.d(ElementsActivity.class.getSimpleName(), "initializeSpinnerFiltering");

        ArrayAdapter<CharSequence> charSequenceArrayAdapterFiltering = ArrayAdapter.createFromResource(this, R.array.FilteringArray, android.R.layout.simple_spinner_item);
        charSequenceArrayAdapterFiltering.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinnerFiltering = this.findViewById(R.id.spinnerFiltering);
        spinnerFiltering.setAdapter(charSequenceArrayAdapterFiltering);
        spinnerFiltering.setOnItemSelectedListener(this.getOnItemSelectedListenerFiltering());

        if (!ElementsActivity.getFilteringSharedPreferencesValue(this).isEmpty()) {
            for (int i = 0; i < this.getResources().getStringArray(R.array.FilteringArray).length; i++) {
                if (ElementsActivity.getFilteringSharedPreferencesValue(this).equals(spinnerFiltering.getResources().getStringArray(R.array.FilteringArray)[i])) {
                    spinnerFiltering.setSelection(i);
                }
            }
        } else {
            spinnerFiltering.setSelection(0);
        }
    }

    private Comparator<ChemicalElement> getChemicalElementComparator() {
        Log.d(ElementsActivity.class.getSimpleName(), "getChemicalElementComparator");

        Comparator<ChemicalElement> chemicalElementComparator = ChemicalElement::compareToAndByAtomicNumberASC;

        for (int i = 0; i < this.getResources().getStringArray(R.array.SortingArray).length; i++) {
            if (this.getSorting().equals(this.getResources().getStringArray(R.array.SortingArray)[i])) {
                switch (i) {
                    case 0:
                        chemicalElementComparator = ChemicalElement::compareToAndByAtomicNumberASC;
                        break;
                    case 1:
                        chemicalElementComparator = ChemicalElement::compareToAndByElementNameASC;
                        break;
                    case 2:
                        chemicalElementComparator = ChemicalElement::compareToAndByAtomicNumberDESC;
                        break;
                    case 3:
                        chemicalElementComparator = ChemicalElement::compareToAndByElementNameDESC;
                        break;
                    default:
                        chemicalElementComparator = ChemicalElement::compareToAndByAtomicNumberASC;
                        break;
                }
            }
        }

        return chemicalElementComparator;
    }

    private Predicate<ChemicalElement> getChemicalElementPredicate() {
        Log.d(ElementsActivity.class.getSimpleName(), "getChemicalElementPredicate");

        Predicate<ChemicalElement> chemicalElementPredicate = null;

        for (int i = 0; i < this.getResources().getStringArray(R.array.FilteringArray).length; i++) {
            if (this.getFiltering().equals(this.getResources().getStringArray(R.array.FilteringArray)[i])) {
                switch (i) {
                    case 0:
                        chemicalElementPredicate = null;
                        break;
                    case 1:
                        chemicalElementPredicate = chemicalElement -> chemicalElement.getRadioactive().equals("yes");
                        break;
                    case 2:
                        chemicalElementPredicate = chemicalElement -> chemicalElement.getNatural().equals("yes");
                        break;
                    case 3:
                        chemicalElementPredicate = chemicalElement -> chemicalElement.getMetal().equals("yes");
                        break;
                    case 4:
                        chemicalElementPredicate = chemicalElement -> chemicalElement.getMetalloid().equals("yes");
                        break;
                    case 5:
                        chemicalElementPredicate = chemicalElement -> chemicalElement.getNonmetal().equals("yes");
                        break;
                    default:
                        break;
                }
            }
        }

        return chemicalElementPredicate;
    }

    private void updateRecyclerView() {
        Log.d(ElementsActivity.class.getSimpleName(), "updateRecyclerView");

        this.updateRecyclerView(this.getSpanCount());
    }

    private void updateRecyclerView(int spanCount) {
        Log.d(ElementsActivity.class.getSimpleName(), "updateRecyclerView " + spanCount);

        Comparator<ChemicalElement> chemicalElementComparator = this.getChemicalElementComparator();
        Predicate<ChemicalElement> chemicalElementPredicate = this.getChemicalElementPredicate();

        this.showChemicalElementsInRecyclerView(chemicalElementComparator, chemicalElementPredicate, spanCount);
    }

    private void showChemicalElementsInRecyclerView(Comparator<ChemicalElement> chemicalElementComparator, Predicate<ChemicalElement> chemicalElementPredicate) {
        Log.d(ElementsActivity.class.getSimpleName(), "showChemicalElementsInRecyclerView");

        this.showChemicalElementsInRecyclerView(chemicalElementComparator, chemicalElementPredicate, 1);
    }

    private void showChemicalElementsInRecyclerView(Comparator<ChemicalElement> chemicalElementComparator, Predicate<ChemicalElement> chemicalElementPredicate, int spanCount) {
        Log.d(ElementsActivity.class.getSimpleName(), "showChemicalElementsInRecyclerView" + spanCount);

        if (chemicalElementPredicate == null) {
            PeriodicTableOfElements.getInstance().showChemicalElementsInRecyclerView(this, this.findViewById(R.id.recyclerView), this.findViewById(R.id.group), this.findViewById(R.id.errorTextView), chemicalElementComparator, spanCount);
        } else {
            PeriodicTableOfElements.getInstance().showChemicalElementsInRecyclerView(this, this.findViewById(R.id.recyclerView), this.findViewById(R.id.group), this.findViewById(R.id.errorTextView), chemicalElementComparator, chemicalElementPredicate, spanCount);
        }
    }

    private static String getSortingSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(),
                "getSortingSharedPreferencesValue " + activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getString(ElementsActivity.getSortingSharedPreferences(), ""));

        return activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getString(ElementsActivity.getSortingSharedPreferences(), "");
    }

    private static void setSortingSharedPreferencesValue(Activity activity, String sorting) {
        Log.d(ElementsActivity.class.getSimpleName(), "setSortingSharedPreferencesValue " + sorting);

        SharedPreferences sharedPreferences = activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ElementsActivity.getSortingSharedPreferences(), sorting);
        editor.apply();

        ElementsActivity.getSortingSharedPreferencesValue(activity);
    }

    private static void resetSortingSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(), "resetSortingSharedPreferencesValue");

        ElementsActivity.setSortingSharedPreferencesValue(activity, "");
    }

    private static String getFilteringSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(),
                "getFilteringSharedPreferencesValue " + activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getString(ElementsActivity.getFilteringSharedPreferences(), ""));

        return activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getString(ElementsActivity.getFilteringSharedPreferences(), "");
    }

    private static void setFilteringSharedPreferencesValue(Activity activity, String filtering) {
        Log.d(ElementsActivity.class.getSimpleName(), "setFilteringSharedPreferencesValue " + filtering);

        SharedPreferences sharedPreferences = activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ElementsActivity.getFilteringSharedPreferences(), filtering);
        editor.apply();

        ElementsActivity.getFilteringSharedPreferencesValue(activity);
    }

    private static void resetFilteringSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(), "resetFilteringSharedPreferencesValue");

        ElementsActivity.setFilteringSharedPreferencesValue(activity, "");
    }

    private static int getSpanCountSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(),
                "getSpanCountSharedPreferencesValue " + activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getInt(ElementsActivity.getSpanCountSharedPreferences(), 1));

        return activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE).getInt(ElementsActivity.getSpanCountSharedPreferences(), 1);
    }

    private static void setSpanCountSharedPreferencesValue(Activity activity, int spanCount) {
        Log.d(ElementsActivity.class.getSimpleName(), "setSpanCountSharedPreferencesValue " + spanCount);

        SharedPreferences sharedPreferences = activity.getSharedPreferences(ElementsActivity.class.getSimpleName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(ElementsActivity.getSpanCountSharedPreferences(), spanCount);
        editor.apply();

        ElementsActivity.getSpanCountSharedPreferencesValue(activity);
    }

    private static void resetSpanCountSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(), "resetSpanCountSharedPreferencesValue");

        ElementsActivity.setSpanCountSharedPreferencesValue(activity, 1);
    }

    public static void resetSortingAndFilteringAndSpanCountSharedPreferencesValue(Activity activity) {
        Log.d(ElementsActivity.class.getSimpleName(), "resetSortingAndFilteringAndSpanCountSharedPreferencesValue");

        ElementsActivity.resetSortingSharedPreferencesValue(activity);
        ElementsActivity.resetFilteringSharedPreferencesValue(activity);
        ElementsActivity.resetSpanCountSharedPreferencesValue(activity);
    }

    private void setSpanCountAndSpanCountSharedPreferencesValue(int spanCount) {
        Log.d(ElementsActivity.class.getSimpleName(), "setSpanCountAndSpanCountSharedPreferencesValue");

        this.setSpanCount(spanCount);
        ElementsActivity.setSpanCountSharedPreferencesValue(this, spanCount);
    }

    private String getSorting() {
        return sorting;
    }

    private void setSorting(String sorting) {
        this.sorting = sorting;
    }

    private String getFiltering() {
        return filtering;
    }

    private void setFiltering(String filtering) {
        this.filtering = filtering;
    }

    private int getSpanCount() {
        return spanCount;
    }

    private void setSpanCount(int spanCount) {
        this.spanCount = spanCount;
    }

    private static String getSortingSharedPreferences() {
        return SORTING_SHARED_PREFERENCES;
    }

    private static String getFilteringSharedPreferences() {
        return FILTERING_SHARED_PREFERENCES;
    }

    private static String getSpanCountSharedPreferences() {
        return SPAN_COUNT_SHARED_PREFERENCES;
    }

    private AdapterView.OnItemSelectedListener getOnItemSelectedListenerSorting() {
        return onItemSelectedListenerSorting;
    }

    private AdapterView.OnItemSelectedListener getOnItemSelectedListenerFiltering() {
        return onItemSelectedListenerFiltering;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_activity_element, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuOneColumn:
                this.setSpanCountAndSpanCountSharedPreferencesValue(1);
                break;
            case R.id.menuTwoColumn:
                this.setSpanCountAndSpanCountSharedPreferencesValue(2);
                break;
        }

        this.updateRecyclerView();

        return true;
    }
}