package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.PeriodicTableOfElements;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        new Thread(PeriodicTableOfElements::getInstance).start();
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.getImageWithPicassoAndRoundedCorners((ImageView) this.findViewById(R.id.imageView));

        this.findViewById(R.id.buttonShowCompleteElementList).setOnClickListener(v -> this.startActivity(new Intent(this.getApplicationContext(), ElementsActivity.class)));
        this.findViewById(R.id.buttonShowGroupList).setOnClickListener(v -> this.startActivity(new Intent(this.getApplicationContext(), GroupActivity.class)));
        this.findViewById(R.id.buttonShowPeriodList).setOnClickListener(v -> this.startActivity(new Intent(this.getApplicationContext(), PeriodActivity.class)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_activity_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menuAbout:
                this.startActivity(new Intent(this.getApplicationContext(), AboutActivity.class));
                break;
            case R.id.menuMoreInfo:
                this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.periodensystem.info/")));
                break;
        }

        return true;
    }

    private void getImageWithPicassoAndRoundedCorners(ImageView imageView) {
        Picasso.get().setLoggingEnabled(true);

        Picasso.get()
                .load(R.drawable.pse)
                .error(R.drawable.error)
                .placeholder(R.drawable.white)
                .transform(new RoundedCornersTransformation(40, 20))
                .into(imageView);
    }
}