package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.neelpatel.NeelpatelJSONData;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.neelpatel.NeelpatelRepo;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi.RapidAPIJSONData;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi.RapidAPIRepo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Andreas Greiff, Florian Jänisch
 * this class is implementend as singleton
 */
public class PeriodicTableOfElements {
    private static PeriodicTableOfElements instance = null;

    private ArrayList<NeelpatelJSONData> neelpatelJSONDataArrayList = null;
    private ArrayList<RapidAPIJSONData> rapidAPIJSONDataArrayList = null;

    private ArrayList<ChemicalElement> chemicalElementArrayList = new ArrayList<>();
    private boolean chemicalElementArrayListReady = false;

    private boolean apiError = false;
    private String apiErrorText = "";

    private PeriodicTableOfElements() {
        this.getChemicalElementDataFromApi();
    }

    public static synchronized PeriodicTableOfElements getInstance() {
        if (instance == null) {
            synchronized (PeriodicTableOfElements.class) {
                if (instance == null) {
                    instance = new PeriodicTableOfElements();
                }
            }
        }

        return instance;
    }

    private NeelpatelRepo neelpatelRepo = new NeelpatelRepo();

    private Callback<ArrayList<NeelpatelJSONData>> callbackArrayListNeelpatelJSONData = new Callback<ArrayList<NeelpatelJSONData>>() {
        @Override
        public void onResponse(@NotNull Call<ArrayList<NeelpatelJSONData>> call, @NotNull Response<ArrayList<NeelpatelJSONData>> response) {
            PeriodicTableOfElements.this.getChemicalElementsApiOnResponse(response);
        }

        @Override
        public void onFailure(@NotNull Call<ArrayList<NeelpatelJSONData>> call, @NotNull Throwable t) {
            PeriodicTableOfElements.this.getChemicalElementsApiOnFailure();
        }
    };

    private RapidAPIRepo rapidAPIRepo = new RapidAPIRepo();

    private Callback<ArrayList<RapidAPIJSONData>> callbackArrayListRapidAPIJSONData = new Callback<ArrayList<RapidAPIJSONData>>() {
        @Override
        public void onResponse(@NotNull Call<ArrayList<RapidAPIJSONData>> call, @NotNull Response<ArrayList<RapidAPIJSONData>> response) {
            PeriodicTableOfElements.this.getChemicalElementsApiOnResponse(response);
        }

        @Override
        public void onFailure(@NotNull Call<ArrayList<RapidAPIJSONData>> call, @NotNull Throwable t) {
            PeriodicTableOfElements.this.getChemicalElementsApiOnFailure();
        }
    };

    private void getChemicalElementDataFromApi() {
        this.setChemicalElementArrayListReady(false);
        this.setApiError(false);
        this.resetApiErrorText();

        this.getChemicalElementsFromNeelpatelAPI();
        this.getChemicalElementsFromRapidAPI();
    }

    private void getChemicalElementsFromNeelpatelAPI() {
        this.getNeelpatelRepo().getChemicalElements(this.getCallbackArrayListNeelpatelJSONData());
    }

    private void getChemicalElementsFromRapidAPI() {
        this.getRapidAPIRepo().getChemicalElements(this.getCallbackArrayListRapidAPIJSONData());
    }

    private <T> void getChemicalElementsApiOnResponse(@NotNull Response<ArrayList<T>> response) {
        if (response.isSuccessful()) {
            this.getChemicalElementsApiOnResponseIsSuccessful(response);
        } else {
            this.getChemicalElementsApiOnResponseIsNotSuccessful();
        }
    }

    private <T> void getChemicalElementsApiOnResponseIsSuccessful(@NotNull Response<ArrayList<T>> response) {
        Log.d(PeriodicTableOfElements.class.getSimpleName(), "onResponse + isSuccessful");

        ArrayList<T> dataArrayList = response.body();

        if (dataArrayList != null) {
            if (!dataArrayList.isEmpty()) {
                if (dataArrayList.get(0).getClass().getSimpleName().equals(NeelpatelJSONData.class.getSimpleName())) {
                    Log.d(PeriodicTableOfElements.class.getSimpleName(), "setNeelpatelJSONDataArrayList");
                    this.setNeelpatelJSONDataArrayList((ArrayList<NeelpatelJSONData>) dataArrayList);
                }

                if (dataArrayList.get(0).getClass().getSimpleName().equals(RapidAPIJSONData.class.getSimpleName())) {
                    Log.d(PeriodicTableOfElements.class.getSimpleName(), "setRapidAPIJSONDataArrayList");
                    this.setRapidAPIJSONDataArrayList((ArrayList<RapidAPIJSONData>) dataArrayList);
                }

                if (this.getNeelpatelJSONDataArrayList() != null && this.getRapidAPIJSONDataArrayList() != null) {
                    this.combineChemicalElementData();
                }
            } else {
                Log.d(PeriodicTableOfElements.class.getSimpleName(), "onResponse + isSuccessful + response.body() is NOT Null + response.body() isEmpty");
                this.setApiError(true);
                this.setApiErrorText(this.getApiErrorText() + "onResponse\n+\nisSuccessful\n+\nresponse.body()\nis Null\n------------------\n");
            }
        } else {
            Log.d(PeriodicTableOfElements.class.getSimpleName(), "onResponse + isSuccessful + response.body() is Null");
            this.setApiError(true);
            this.setApiErrorText(this.getApiErrorText() + "onResponse\n+\nisSuccessful\n+\nresponse.body()\nis Null\n------------------\n");
        }
    }

    private void getChemicalElementsApiOnResponseIsNotSuccessful() {
        Log.d(PeriodicTableOfElements.class.getSimpleName(), "onResponse + NOT isSuccessful");
        this.setApiError(true);
        this.setApiErrorText(this.getApiErrorText() + "onResponse\n+\nNOT\nisSuccessful\n------------------\n");
    }

    private void getChemicalElementsApiOnFailure() {
        Log.d(PeriodicTableOfElements.class.getSimpleName(), "onFailure");
        this.setApiError(true);
        this.setApiErrorText(this.getApiErrorText() + "onFailure\n------------------\n");
    }

    /**
     * In this method the data from both apis, neelpatel and rapidapi are combined to one chemicalElement
     * The data from both apis is mapped by the atomic number
     */
    private void combineChemicalElementData() {
        Log.d(PeriodicTableOfElements.class.getSimpleName(), "combineData()");

        this.getChemicalElementArrayList().clear();

        for (NeelpatelJSONData neelpatelJSONData : this.getNeelpatelJSONDataArrayList()) {
            for (RapidAPIJSONData rapidAPIJSONData : this.getRapidAPIJSONDataArrayList()) {
                if (neelpatelJSONData.getAtomicNumber() == rapidAPIJSONData.getAtomicNumber()) {
                    this.getChemicalElementArrayList().add(new ChemicalElement(neelpatelJSONData, rapidAPIJSONData));
                    break;
                }
            }
        }

        this.getChemicalElementArrayList().sort(ChemicalElement::compareToAndByAtomicNumberASC);

        this.setChemicalElementArrayListReady(true);
    }

    /**
     * Returns the data filtered by the atomic number
     */
    public ChemicalElement getChemicalElementByAtomicNumber(int atomicNumber) {
        ArrayList<ChemicalElement> chemicalElementArrayList = this.getChemicalElementArrayList();

        Predicate<ChemicalElement> byAtomicNumber = chemicalElement -> atomicNumber == chemicalElement.getAtomicNumber();

        chemicalElementArrayList = (ArrayList<ChemicalElement>) chemicalElementArrayList.stream().filter(byAtomicNumber).collect(Collectors.toList());

        return chemicalElementArrayList.get(0);
    }

    private void showChemicalElementsInRecyclerView(ArrayList<ChemicalElement> chemicalElementArrayList, Context context, RecyclerView recyclerView, Group group, TextView textView, int spanCount) {
        while (true) {
            if (this.isApiError()) {
                Log.d(PeriodicTableOfElements.class.getSimpleName(), "API ERROR");
                textView.setText(this.getApiErrorText());

                textView.setVisibility(View.VISIBLE);
                group.setVisibility(View.GONE);
                break;
            } else {
                textView.setVisibility(View.GONE);
                group.setVisibility(View.VISIBLE);

                if (this.isChemicalElementArrayListReady()) {
                    Log.d(PeriodicTableOfElements.class.getSimpleName(), "Show Chemical Elements In RecyclerView");
                    recyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, RecyclerView.VERTICAL, false));
                    recyclerView.setAdapter(new ChemicalElementAdapter(chemicalElementArrayList, context));

                    if (spanCount == 1) {
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        ((View) recyclerView).getLayoutParams().width = (int) (displayMetrics.widthPixels * 0.4);
                    }

                    if (spanCount == 2) {
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        ((View) recyclerView).getLayoutParams().width = (int) (displayMetrics.widthPixels * 0.75);
                    }

                    break;
                }
            }
        }
    }

    public void showChemicalElementsInRecyclerView(Context context, RecyclerView recyclerView, Group group, TextView textView, int spanCount) {
        this.showChemicalElementsInRecyclerView(context, recyclerView, group, textView, ChemicalElement::compareToAndByAtomicNumberASC, spanCount);
    }

    public void showChemicalElementsInRecyclerView(Context context, RecyclerView recyclerView, Group group, TextView textView, Comparator<ChemicalElement> chemicalElementComparator, int spanCount) {
        this.showChemicalElementsInRecyclerView(this.getChemicalElementArrayList(chemicalElementComparator), context, recyclerView, group, textView, spanCount);
    }

    public void showChemicalElementsInRecyclerView(Context context, RecyclerView recyclerView, Group group, TextView textView, Comparator<ChemicalElement> chemicalElementComparator, Predicate<ChemicalElement> chemicalElementPredicate, int spanCount) {
        this.showChemicalElementsInRecyclerView(this.getChemicalElementArrayList(chemicalElementPredicate, chemicalElementComparator), context, recyclerView, group, textView, spanCount);
    }

    /**
     * this method filter the chemical elements by group and set to data to the given recycler view
     */
    public void showChemicalElementsByGroupInRecyclerView(Context context, RecyclerView recyclerView, Group group, TextView textView, int groupNumber) {
        ArrayList<ChemicalElement> chemicalElementArrayList;

        chemicalElementArrayList = this.getChemicalElementArrayList(chemicalElement -> null != chemicalElement.getGroup());

        chemicalElementArrayList = this.getChemicalElementArrayList(chemicalElementArrayList,
                chemicalElement -> chemicalElement.getGroup().equals(Integer.toString(groupNumber)));

        this.showChemicalElementsInRecyclerView(chemicalElementArrayList, context, recyclerView, group, textView, 1);
    }

    /**
     * this method filter the chemical elements by period and set to data to the given recycler view
     */
    public void showChemicalElementsByPeriodInRecyclerView(Context context, RecyclerView recyclerView, Group group, TextView textView, int periodNumber) {
        ArrayList<ChemicalElement> chemicalElementArrayList;

        chemicalElementArrayList = this.getChemicalElementArrayList(chemicalElement -> null != chemicalElement.getPeriod());

        chemicalElementArrayList = this.getChemicalElementArrayList(chemicalElementArrayList,
                chemicalElement -> chemicalElement.getPeriod().equals(Integer.toString(periodNumber)));

        this.showChemicalElementsInRecyclerView(chemicalElementArrayList, context, recyclerView, group, textView, 1);
    }

    /**
     * in this method the background of the chemical element overview is set to its color collected from the api
     */
    public static void setChemicalElementBackgroundCpkHexColor(FrameLayout frameLayoutChemicalElement, ChemicalElement chemicalElement) {
        if (chemicalElement.getCpkHexColor().length() == 6) {
            frameLayoutChemicalElement.getBackground().setTint(Color.parseColor("#" + chemicalElement.getCpkHexColor()));
        } else {
            String cpkHexColor = "FFFFFF";

            if (chemicalElement.getAtomicNumber() == 9) {
                cpkHexColor = "90E050";
            }

            frameLayoutChemicalElement.getBackground().setTint(Color.parseColor("#" + cpkHexColor));
        }
    }

    private NeelpatelRepo getNeelpatelRepo() {
        return neelpatelRepo;
    }

    private Callback<ArrayList<NeelpatelJSONData>> getCallbackArrayListNeelpatelJSONData() {
        return callbackArrayListNeelpatelJSONData;
    }

    private RapidAPIRepo getRapidAPIRepo() {
        return rapidAPIRepo;
    }

    private Callback<ArrayList<RapidAPIJSONData>> getCallbackArrayListRapidAPIJSONData() {
        return callbackArrayListRapidAPIJSONData;
    }

    private ArrayList<NeelpatelJSONData> getNeelpatelJSONDataArrayList() {
        return neelpatelJSONDataArrayList;
    }

    private void setNeelpatelJSONDataArrayList(ArrayList<NeelpatelJSONData> neelpatelJSONDataArrayList) {
        this.neelpatelJSONDataArrayList = neelpatelJSONDataArrayList;
    }

    private ArrayList<RapidAPIJSONData> getRapidAPIJSONDataArrayList() {
        return rapidAPIJSONDataArrayList;
    }

    private void setRapidAPIJSONDataArrayList(ArrayList<RapidAPIJSONData> rapidAPIJSONDataArrayList) {
        this.rapidAPIJSONDataArrayList = rapidAPIJSONDataArrayList;
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList() {
        return chemicalElementArrayList;
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(Comparator<ChemicalElement> chemicalElementComparator) {
        ArrayList<ChemicalElement> chemicalElementArrayList = this.getChemicalElementArrayList();

        chemicalElementArrayList.sort(chemicalElementComparator);

        return chemicalElementArrayList;
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(ArrayList<ChemicalElement> chemicalElementArrayList, Comparator<ChemicalElement> chemicalElementComparator) {
        chemicalElementArrayList.sort(chemicalElementComparator);

        return chemicalElementArrayList;
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(Predicate<ChemicalElement> chemicalElementPredicate) {
        return this.getChemicalElementArrayList(chemicalElementPredicate, ChemicalElement::compareToAndByAtomicNumberASC);
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(ArrayList<ChemicalElement> chemicalElementArrayList, Predicate<ChemicalElement> chemicalElementPredicate) {
        return this.getChemicalElementArrayList(chemicalElementArrayList, chemicalElementPredicate, ChemicalElement::compareToAndByAtomicNumberASC);
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(Predicate<ChemicalElement> chemicalElementPredicate, Comparator<ChemicalElement> chemicalElementComparator) {
        return (ArrayList<ChemicalElement>) this.getChemicalElementArrayList(chemicalElementComparator).stream().filter(chemicalElementPredicate).collect(Collectors.toList());
    }

    private ArrayList<ChemicalElement> getChemicalElementArrayList(ArrayList<ChemicalElement> chemicalElementArrayList, Predicate<ChemicalElement> chemicalElementPredicate, Comparator<ChemicalElement> chemicalElementComparator) {
        return (ArrayList<ChemicalElement>) this.getChemicalElementArrayList(chemicalElementArrayList, chemicalElementComparator).stream().filter(chemicalElementPredicate).collect(Collectors.toList());
    }

    public boolean isChemicalElementArrayListReady() {
        return chemicalElementArrayListReady;
    }

    private void setChemicalElementArrayListReady(boolean chemicalElementArrayListReady) {
        this.chemicalElementArrayListReady = chemicalElementArrayListReady;
    }

    public boolean isApiError() {
        return apiError;
    }

    private void setApiError(boolean apiError) {
        this.apiError = apiError;
    }

    private String getApiErrorText() {
        return apiErrorText;
    }

    private void setApiErrorText(String apiErrorText) {
        this.apiErrorText = apiErrorText;
    }

    private void resetApiErrorText() {
        this.setApiErrorText("API ERROR\n------------------\n");
    }
}