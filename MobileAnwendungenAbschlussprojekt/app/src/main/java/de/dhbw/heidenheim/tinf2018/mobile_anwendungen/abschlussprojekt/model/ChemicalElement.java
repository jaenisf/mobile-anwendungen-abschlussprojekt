package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.neelpatel.NeelpatelJSONData;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi.RapidAPIJSONData;

/**
 * @author Andreas Greiff, Florian Jänisch
 * this class represent a specific chemical Element
 * This class implements a Method for each chemical Elements detail (in the methods, the data will be collected from neelpatel or from rapidAPI
 */
public class ChemicalElement {
    private NeelpatelJSONData neelpatelJSONDataList;
    private RapidAPIJSONData rapidAPIJSONDataList;

    public ChemicalElement(NeelpatelJSONData neelpatelJSONDataList, RapidAPIJSONData rapidAPIJSONDataList) {
        this.neelpatelJSONDataList = neelpatelJSONDataList;
        this.rapidAPIJSONDataList = rapidAPIJSONDataList;
    }

    public NeelpatelJSONData getNeelpatelJSONDataList() {
        return neelpatelJSONDataList;
    }

    public RapidAPIJSONData getRapidAPIJSONDataList() {
        return rapidAPIJSONDataList;
    }

    public int getAtomicNumber() {
        return this.getNeelpatelJSONDataList().getAtomicNumber();
    }

    public String getElementSymbol() {
        return this.getNeelpatelJSONDataList().getSymbol();
    }

    public String getElementName() {
        return this.getNeelpatelJSONDataList().getName();
    }

    public String getAtomicMass() {
        return this.getRapidAPIJSONDataList().getAtomicMass();
    }

    public String getCpkHexColor() {
        return this.getNeelpatelJSONDataList().getCpkHexColor();
    }

    public String getGroup() {
        return this.getRapidAPIJSONDataList().getGroup();
    }

    public String getBoilingPoint() {
        return this.getRapidAPIJSONDataList().getBoilingPoint();
    }

    public String getNumberOfIsotopes() {
        return this.getRapidAPIJSONDataList().getNumberOfIsotopes();
    }

    public String getNumberOfElectrons() {
        return this.getRapidAPIJSONDataList().getNumberofElectrons();
    }

    public String getNumberOfNeutrons() {
        return this.getRapidAPIJSONDataList().getNumberofNeutrons();
    }

    public String getNumberOfProtons() {
        return this.getRapidAPIJSONDataList().getNumberofProtons();
    }

    public String getPeriod() {
        return this.getRapidAPIJSONDataList().getPeriod();
    }

    public String getPhase() {
        return this.getRapidAPIJSONDataList().getPhase();
    }

    public String getRadioactive() {
        return this.getRapidAPIJSONDataList().getRadioactive();
    }

    public String getType() {
        return this.getRapidAPIJSONDataList().getType();
    }

    public String getYear() {
        return this.getRapidAPIJSONDataList().getYear();
    }

    public String getMetal() {
        return this.getRapidAPIJSONDataList().getMetal();
    }

    public String getMetalloid() {
        return this.getRapidAPIJSONDataList().getMetalloid();
    }

    public String getNatural() {
        return this.getRapidAPIJSONDataList().getNatural();
    }

    public String getNonmetal() {
        return this.getRapidAPIJSONDataList().getNonmetal();
    }

    public String getDiscoverer() {
        return this.getRapidAPIJSONDataList().getDiscoverer();
    }

    public String getElectronegativity() {
        return this.getRapidAPIJSONDataList().getElectronegativity();
    }

    public String getAtomicRadius() {
        return this.getNeelpatelJSONDataList().getAtomicRadius();
    }

    public String getBondingType() {
        return this.getNeelpatelJSONDataList().getBondingType();
    }

    public String getDensity() {
        return this.getNeelpatelJSONDataList().getDensity();
    }

    public String getMeltingPoint() {
        return this.getNeelpatelJSONDataList().getMeltingPoint();
    }

    public String getIonizationEnergy() {
        return this.getNeelpatelJSONDataList().getIonizationEnergy();
    }

    public String getOxidationStates() {
        return this.getNeelpatelJSONDataList().getOxidationStates();
    }

    public String getStandardSate() {
        return this.getNeelpatelJSONDataList().getStandardState();
    }

    public String getVanDelWaalsRadius() {
        return this.getNeelpatelJSONDataList().getVanDelWaalsRadius();
    }

    public String getNumberOfShells() {
        return this.getRapidAPIJSONDataList().getNumberofShells();
    }

    public String getNumberOfValence() {
        return this.getRapidAPIJSONDataList().getNumberofValence();
    }

    public String getSpecificHeat() {
        return this.getRapidAPIJSONDataList().getSpecificHeat();
    }

    public int compareToAndByAtomicNumberASC(ChemicalElement chemicalElement) {
        return Integer.compare(this.getAtomicNumber(), chemicalElement.getAtomicNumber());
    }

    public int compareToAndByAtomicNumberDESC(ChemicalElement chemicalElement) {
        return Integer.compare(chemicalElement.getAtomicNumber(), this.getAtomicNumber());
    }

    public int compareToAndByElementNameASC(ChemicalElement chemicalElement) {
        return this.getElementName().compareTo(chemicalElement.getElementName());
    }

    public int compareToAndByElementNameDESC(ChemicalElement chemicalElement) {
        return chemicalElement.getElementName().compareTo(this.getElementName());
    }
}