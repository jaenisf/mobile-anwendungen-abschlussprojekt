package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
@Dao
public interface GroupDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Group group);

    @Query("SELECT * FROM GroupTable")
    List<Group> getAllGroupInfo();

    @Query("SELECT * FROM GroupTable WHERE group_number = :number AND language = :language LIMIT 1")
    Group getSpecificGroup(int number, String language);

    @Query("DELETE FROM GroupTable")
    void deleteAllEntries();
}