package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public interface RapidAPI {
    @Headers({
            "x-rapidapi-key: 0c120364a7msh64fda820cb1bacep1086b0jsnfbd3237c9fdc",
            "x-rapidapi-host: periodic-table-of-elements.p.rapidapi.com"
    })
    @GET("elements")
    Call<ArrayList<RapidAPIJSONData>> getChemicalElements();
}