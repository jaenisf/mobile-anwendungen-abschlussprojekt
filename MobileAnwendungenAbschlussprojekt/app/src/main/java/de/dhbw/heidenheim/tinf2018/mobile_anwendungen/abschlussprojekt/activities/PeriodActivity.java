package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.util.Locale;

import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.R;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database.Period;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database.PeriodDao;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database.PeriodDatabase;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.listener.OnSwipeTouchListener;
import de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.model.PeriodicTableOfElements;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class PeriodActivity extends AppCompatActivity {
    private int currentPeriodNumber = 1;
    private PeriodDao periodDao;
    private TextView eightPeriodTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_period);

        this.setEightPeriodTextView(this.findViewById(R.id.periodEightTextView));
        this.getEightPeriodTextView().setVisibility(View.INVISIBLE);

        this.initializeDatabase();
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.showChemicalElementsByPeriod();

        this.findViewById(R.id.scrollView).setOnTouchListener(new OnSwipeTouchListener(PeriodActivity.this) {
            public void onSwipeRight(Context context) {
                derementCurrentPeriodNumber();
            }

            public void onSwipeLeft(Context context) {
                incrementCurrentPeriodNumber();
            }

            public void onSwipeTop(Context context) {
            }

            public void onSwipeBottom(Context context) {
            }
        });

        this.findViewById(R.id.arrowLeft).setOnClickListener(v -> derementCurrentPeriodNumber());
        this.findViewById(R.id.arrowRight).setOnClickListener(v -> incrementCurrentPeriodNumber());
    }

    private void initializeDatabase() {
        // Create Database Objects
        PeriodDatabase periodDatabase = Room.databaseBuilder(this, PeriodDatabase.class, "PeriodTable").allowMainThreadQueries().build();
        this.setPeriodDao(periodDatabase.periodDao());

        // If database is empty, enter standard values

        // SET false TO true | only needed, when db values changed --> to clear database table
        if (false) {
            this.getPeriodDao().deleteAllEntries();
        }

        if (this.getPeriodDao().getAllPeriodInfo() == null || this.getPeriodDao().getAllPeriodInfo().isEmpty()) {
            this.insertStandardDatabaseValues();
        }
    }

    private void showChemicalElementsByPeriod(int periodNumber) {
        PeriodicTableOfElements.getInstance().showChemicalElementsByPeriodInRecyclerView(this, this.findViewById(R.id.recyclerView), this.findViewById(R.id.group), this.findViewById(R.id.errorTextView), periodNumber);
    }

    /**
     * show the header depending of the selected period id and system language
     */
    @SuppressLint("SetTextI18n")
    private void showChemicalElementsByPeriod() {
        String used_language = "en";

        if (Locale.getDefault().getDisplayLanguage().equals("Deutsch")) {
            used_language = "de";
        }

        Period period = this.getPeriodDao().getSpecificPeriod(this.getCurrentPeriodNumber(), used_language);

        Log.d("Period", Integer.toString(this.getCurrentPeriodNumber()));
        ((TextView) this.findViewById(R.id.textViewTitle)).setText(period.getPeriodName() + "\n(" + this.getString(R.string.PeriodElectronShell, period.getElectronShell()) + ")");

        this.showChemicalElementsByPeriod(this.getCurrentPeriodNumber());
    }

    /**
     * increment the period ID
     */
    private void incrementCurrentPeriodNumber() {
        if (this.getCurrentPeriodNumber() == 8) {
            this.setCurrentPeriodNumber(1);
        } else {
            this.setCurrentPeriodNumber(getCurrentPeriodNumber() + 1);
        }

        if (getCurrentPeriodNumber() == 8) {
            this.getEightPeriodTextView().setVisibility(View.VISIBLE);
        } else {
            this.getEightPeriodTextView().setVisibility(View.GONE);
        }

        this.showChemicalElementsByPeriod();
    }

    /**
     * decrement the period ID
     */
    private void derementCurrentPeriodNumber() {
        if (this.getCurrentPeriodNumber() == 1) {
            this.setCurrentPeriodNumber(8);
        } else {
            this.setCurrentPeriodNumber(getCurrentPeriodNumber() - 1);
        }

        if (getCurrentPeriodNumber() == 8) {
            this.getEightPeriodTextView().setVisibility(View.VISIBLE);
        } else {
            this.getEightPeriodTextView().setVisibility(View.GONE);
        }

        this.showChemicalElementsByPeriod();
    }

    private int getCurrentPeriodNumber() {
        return currentPeriodNumber;
    }

    private void setCurrentPeriodNumber(int currentPeriodNumber) {
        this.currentPeriodNumber = currentPeriodNumber;
    }

    public PeriodDao getPeriodDao() {
        return periodDao;
    }

    public void setPeriodDao(PeriodDao periodDao) {
        this.periodDao = periodDao;
    }

    public TextView getEightPeriodTextView() {
        return eightPeriodTextView;
    }

    public void setEightPeriodTextView(TextView eightPeriodTextView) {
        this.eightPeriodTextView = eightPeriodTextView;
    }

    /**
     * Inserting the needed period definitions into the database
     */
    private void insertStandardDatabaseValues() {
        Period period;

        //Periods - Language: German
        period = new Period(1, 1, "1. Periode", "K", "de");
        periodDao.insert(period);
        period = new Period(2, 2, "2. Periode", "L", "de");
        periodDao.insert(period);
        period = new Period(3, 3, "3. Periode", "M", "de");
        periodDao.insert(period);
        period = new Period(4, 4, "4. Periode", "N", "de");
        periodDao.insert(period);
        period = new Period(5, 5, "5. Periode", "O", "de");
        periodDao.insert(period);
        period = new Period(6, 6, "6. Periode", "P", "de");
        periodDao.insert(period);
        period = new Period(7, 7, "7. Periode", "Q", "de");
        periodDao.insert(period);
        period = new Period(8, 8, "8. Periode", "", "de");
        periodDao.insert(period);

        //Periods - Language: English
        period = new Period(9, 1, "1st Period", "K", "en");
        periodDao.insert(period);
        period = new Period(10, 2, "2nd Period", "L", "en");
        periodDao.insert(period);
        period = new Period(11, 3, "3rd Period", "M", "en");
        periodDao.insert(period);
        period = new Period(12, 4, "4th Period", "N", "en");
        periodDao.insert(period);
        period = new Period(13, 5, "5th Period", "O", "en");
        periodDao.insert(period);
        period = new Period(14, 6, "6th Period", "P", "en");
        periodDao.insert(period);
        period = new Period(15, 7, "7th Period", "Q", "en");
        periodDao.insert(period);
        period = new Period(16, 8, "8th Period", "", "en");
        periodDao.insert(period);
    }
}