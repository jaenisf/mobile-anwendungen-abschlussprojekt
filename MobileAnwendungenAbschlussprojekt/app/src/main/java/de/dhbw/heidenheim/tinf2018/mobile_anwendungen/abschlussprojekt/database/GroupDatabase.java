package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
@Database(entities = {Group.class}, version = 1, exportSchema = false)
public abstract class GroupDatabase extends RoomDatabase {
    public abstract GroupDao groupDao();
}