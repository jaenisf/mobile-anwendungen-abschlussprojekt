package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class SplashScreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(this::executeSplashScreenFunctionality, 1000);
    }

    private void executeSplashScreenFunctionality() {
        ElementsActivity.resetSortingAndFilteringAndSpanCountSharedPreferencesValue(this);
        this.startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
        this.finish();
    }
}