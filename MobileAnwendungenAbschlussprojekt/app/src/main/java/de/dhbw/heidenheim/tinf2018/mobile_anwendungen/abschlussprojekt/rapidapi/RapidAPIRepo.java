package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.rapidapi;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class RapidAPIRepo {
    private final static String URL = "https://periodic-table-of-elements.p.rapidapi.com/";

    private RapidAPI rapidAPI;
    private Retrofit retrofit;

    public RapidAPIRepo() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        this.setRetrofit(new Retrofit.Builder()
                .baseUrl(URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build());

        this.setRapidAPI(this.getRetrofit().create(RapidAPI.class));
    }

    public void getChemicalElements(Callback<ArrayList<RapidAPIJSONData>> callback) {
        Call<ArrayList<RapidAPIJSONData>> call = this.getRapidAPI().getChemicalElements();
        call.enqueue(callback);
    }

    private RapidAPI getRapidAPI() {
        return rapidAPI;
    }

    private void setRapidAPI(RapidAPI rapidAPI) {
        this.rapidAPI = rapidAPI;
    }

    private Retrofit getRetrofit() {
        return retrofit;
    }

    private void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }
}