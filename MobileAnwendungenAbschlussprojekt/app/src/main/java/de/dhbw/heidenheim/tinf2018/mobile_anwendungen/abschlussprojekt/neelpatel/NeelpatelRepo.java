package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.neelpatel;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
public class NeelpatelRepo {
    private final static String URL = "https://periodic-table-of-elements-api.herokuapp.com/";

    private NeelpatelAPI neelpatelAPI;
    private Retrofit retrofit;

    public NeelpatelRepo() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        this.setRetrofit(new Retrofit.Builder()
                .baseUrl(URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build());

        this.setNeelpatelAPI(this.getRetrofit().create(NeelpatelAPI.class));
    }

    public void getChemicalElements(Callback<ArrayList<NeelpatelJSONData>> callback) {
        Call<ArrayList<NeelpatelJSONData>> call = this.getNeelpatelAPI().getChemicalElements();
        call.enqueue(callback);
    }

    private NeelpatelAPI getNeelpatelAPI() {
        return neelpatelAPI;
    }

    private void setNeelpatelAPI(NeelpatelAPI neelpatelAPI) {
        this.neelpatelAPI = neelpatelAPI;
    }

    private Retrofit getRetrofit() {
        return retrofit;
    }

    private void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }
}
