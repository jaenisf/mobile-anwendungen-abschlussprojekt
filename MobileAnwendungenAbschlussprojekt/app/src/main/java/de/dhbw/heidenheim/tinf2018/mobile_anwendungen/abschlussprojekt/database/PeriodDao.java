package de.dhbw.heidenheim.tinf2018.mobile_anwendungen.abschlussprojekt.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

/**
 * @author Andreas Greiff, Florian Jänisch
 */
@Dao
public interface PeriodDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Period period);

    @Query("SELECT * FROM PeriodTable")
    List<Period> getAllPeriodInfo();

    @Query("SELECT * FROM PeriodTable WHERE period_number = :number AND language = :language LIMIT 1")
    Period getSpecificPeriod(int number, String language);

    @Query("DELETE FROM PeriodTable")
    void deleteAllEntries();
}